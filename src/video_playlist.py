# The starting code provided by Google
# Implemented by Michal Broos during the Internship Experience UK 2021

"""A video playlist class."""


class Playlist:
    """A class used to represent a Playlist."""

    def __init__(self, name):
        self._name = name
        self._videos = {}

    def add_video(self, video_id, video):
        added = False
        if not self._videos.get(video_id):
            self._videos[video_id] = video
            added = True
        return added
    
    def remove_video(self, video_id):
        removed = False
        if video_id in self._videos:
            del self._videos[video_id]
            removed = True
        return removed
    
    def show(self, user_entered_name):
        print("Showing playlist: " + user_entered_name)
        videos = self._videos
        if videos:
            for video_id in videos:
                video = videos[video_id]
                print(video, end="")
                if video.flag:
                    print(" - FLAGGED" + video.print_flag(), end="")
                print()
        else:
            print("No videos here yet")

    def clear(self, user_entered_name):
        if self._videos:
            print("Successfully removed all videos from " + user_entered_name)
            self._videos = {}

    @property
    def name(self):
        return self._name
    
    @property
    def videos(self):
        return self._videos
