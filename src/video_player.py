# The starting code provided by Google
# Implemented by Michal Broos during the Internship Experience UK 2021

"""A video player class."""

import random
from src.video_playlist import Playlist
from .video_library import VideoLibrary


class VideoPlayer:
    """A class used to represent a Video Player."""
    Playing, Paused, Stopped = range(3)

    def __init__(self):
        self._video_library = VideoLibrary()
        self._video_playing = None
        self._status = self.Stopped
        self._playlists = {}

    def _get_all_videos(self):
        return self._video_library.get_all_videos()

    def _get_video(self, video_id):
        return self._video_library.get_video(video_id)

    def number_of_videos(self):
        num_videos = len(self._get_all_videos())
        print(f"{num_videos} videos in the library")

    def show_all_videos(self):
        """Returns all videos."""
        print("Here's a list of all available videos:")
        sorted_videos = []
        for video in self._get_all_videos():
            sorted_videos.append((video.title, video))
        for _, video in sorted(sorted_videos):
            print(video, end="")
            if video.flag:
                print(" - FLAGGED" + video.print_flag(), end="")
            print()

    def print_playing_video(self, video):
        print("Playing video: " + video.title)
    
    def print_stopping_video(self):
        print("Stopping video: " + self._video_playing.title)
        
    def play_video(self, video_id):
        """Plays the respective video.

        Args:
            video_id: The video_id to be played.
        """
        video = self._get_video(video_id)
        if not video:
            print("Cannot play video: Video does not exist")
        elif not video.flag:
            if self._status != self.Stopped:
                self.print_stopping_video()
            self.print_playing_video(video)
            self._video_playing = video
            self._status = self.Playing
        else:
            print("Cannot play video: Video is currently"
                  " flagged" + video.print_flag())

    def stop_video(self):
        """Stops the current video."""
        if self._status != self.Stopped:
            self.print_stopping_video()
            self._video_playing = None
            self._status = self.Stopped
        else:
            print("Cannot stop video: No video is currently playing")

    def play_random_video(self):
        """Plays a random video from the video library."""
        available_videos = list(filter(lambda v: v.flag == "",
                                  self._get_all_videos()))
        if available_videos:
            random_video = random.choice(available_videos)
            self.play_video(random_video.video_id)
        else:
            print("No videos available")

    def pause_video(self):
        """Pauses the current video."""
        if self._status == self.Playing:
            print("Pausing video: " + self._video_playing.title)
            self._status = self.Paused
        elif self._status == self.Paused:
            print("Video already paused: " + self._video_playing.title)
        else:
            print("Cannot pause video: No video is currently playing")

    def continue_video(self):
        """Resumes playing the current video."""
        if self._status == self.Paused:
            print("Continuing video: " + self._video_playing.title)
            self._status = self.Playing
        elif self._status == self.Playing:
            print("Cannot continue video: Video is not paused")
        else:
            print("Cannot continue video: No video is currently playing")

    def show_playing(self):
        """Displays video currently playing."""
        if self._status == self.Stopped:
            print("No video is currently playing")
        else:
            print("Currently playing: " + str(self._video_playing), end="")
            if self._status == self.Paused:
                print(" - PAUSED")
            else:
                print()

    def get_playlist(self, playlist_name):
        return self._playlists.get(playlist_name.lower())
        
    def create_playlist(self, playlist_name):
        """Creates a playlist with a given name.

        Args:
            playlist_name: The playlist name.
        """
        if self.get_playlist(playlist_name):
            print("Cannot create playlist: A playlist with the same name "
                  "already exists")
        else:
            print("Successfully created new playlist: " + playlist_name)
            self._playlists[playlist_name.lower()] = Playlist(playlist_name)

    def show_all_playlists(self):
        """Display all playlists."""
        if self._playlists:
            print("Showing all playlists:")
            for playlist in sorted(self._playlists):
                print(self._playlists.get(playlist).name)
        else:
            print("No playlists exist yet")

    def show_playlist(self, playlist_name):
        """Display all videos in a playlist with a given name.

        Args:
            playlist_name: The playlist name.
        """
        playlist = self.get_playlist(playlist_name)
        if playlist:
            playlist.show(playlist_name)
        else:
            print("Cannot show playlist " + playlist_name + ": "
                  "Playlist does not exist")

    def add_to_playlist(self, playlist_name, video_id):
        """Adds a video to a playlist with a given name.

        Args:
            playlist_name: The playlist name.
            video_id: The video_id to be added.
        """
        self._add_or_remove_handler(playlist_name, video_id, True)

    def _add_or_remove_handler(self, playlist_name, video_id, add):
        def print_cannot():
            if add:
                return "Cannot add video to " + playlist_name + ": "
            else:
                return "Cannot remove video from " + playlist_name + ": "
        
        video = self._get_video(video_id)
        playlist = self.get_playlist(playlist_name)

        if video and video.flag and add:
            print(print_cannot() + "Video is currently"
                  " flagged" + video.print_flag())
        elif not playlist:
            print(print_cannot() + "Playlist does not exist")
        elif not video:
            print(print_cannot() + "Video does not exist")
        elif add:
            added = playlist.add_video(video_id, video)
            if added:
                print("Added video to " + playlist_name + ":"
                      " " + video.title)
            else:
                print(print_cannot() + "Video already added")
        else:
            removed = playlist.remove_video(video_id)
            if removed:
                print("Removed video from " + playlist_name + ":"
                      " " + video.title)
            else:
                print(print_cannot() + "Video is not in playlist")

    def remove_from_playlist(self, playlist_name, video_id):
        """Removes a video to a playlist with a given name.

        Args:
            playlist_name: The playlist name.
            video_id: The video_id to be removed.
        """
        self._add_or_remove_handler(playlist_name, video_id, False)

    def clear_playlist(self, playlist_name):
        """Removes all videos from a playlist with a given name.

        Args:
            playlist_name: The playlist name.
        """
        playlist = self.get_playlist(playlist_name)
        if playlist:
            playlist.clear(playlist_name)
        else:
            print("Cannot clear playlist " + playlist_name + ":"
                  " " + "Playlist does not exist")

    def delete_playlist(self, playlist_name):
        """Deletes a playlist with a given name.

        Args:
            playlist_name: The playlist name.
        """
        playlist = self.get_playlist(playlist_name)
        if playlist:
            del self._playlists[playlist_name]
            print("Deleted playlist: " + playlist_name)
        else:
            print("Cannot delete playlist " + playlist_name + ":"
                  " " + "Playlist does not exist")

    def search_videos(self, search_term):
        """Display all the videos whose titles contain the search_term.

        Args:
            search_term: The query to be used in search.
        """
        videos = []
        for video in self._get_all_videos():
            if not video.flag and search_term.lower() in video.title.lower():
                videos.append(video)
        self._handle_search_results(videos, search_term)

    def _handle_search_results(self, videos, query):
        videos.sort(key=lambda v: v.title)
        if videos:
            print("Here are the results for " + query + ":")
            for i, video in enumerate(videos, start=1):
                print(str(i) + ") " + str(video))
            print("Would you like to play any of the above? If yes, specify the"
                  " number of the video.\nIf your answer is not a valid number,"
                  " we will assume it's a no.")
            answer = input()
            try:
                answer = int(answer)
            except ValueError:
                return
            if answer > 0 and answer <= len(videos):
                self.play_video(videos[answer-1].video_id)
        else:
            print("No search results for " + query)

    def search_videos_tag(self, video_tag):
        """Display all videos whose tags contains the provided tag.

        Args:
            video_tag: The video tag to be used in search.
        """
        if "#" not in video_tag:
            print("No search results for " + video_tag)
        else:
            videos = []
            for video in self._get_all_videos():
                if not video.flag and video_tag in video.tags:
                    videos.append(video)
            self._handle_search_results(videos, video_tag)

    def flag_video(self, video_id, flag_reason=""):
        """Mark a video as flagged.

        Args:
            video_id: The video_id to be flagged.
            flag_reason: Reason for flagging the video.
        """
        def print_cannot(reason):
            print("Cannot flag video: Video " + reason)

        video = self._get_video(video_id)
        if video:
            if video.flag:
                print_cannot("is already flagged")
                return
            elif flag_reason:
                video.flag = flag_reason
            else:
                video.flag = "Not supplied"
            if self._status != self.Stopped and video == self._video_playing:
                self.stop_video()
            print("Successfully flagged video:"
                  " " + video.title + video.print_flag())
        else:
            print_cannot("does not exist")

    def allow_video(self, video_id):
        """Removes a flag from a video.

        Args:
            video_id: The video_id to be allowed again.
        """
        def print_cannot(reason):
            print("Cannot remove flag from video: Video " + reason)

        video = self._get_video(video_id)
        if video:
            if video.flag:
                video.flag = ""
                print("Successfully removed flag from video: " + video.title)
            else:
                print_cannot("is not flagged")
        else:
            print_cannot("does not exist")
